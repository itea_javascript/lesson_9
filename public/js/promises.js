/*
  PROMISES
  Способ организации асинзронного кода.
*/

  // PROMISES
  // Promise status : Pending | Fulfilled | Rejected

  /*
    FIRST PROMISE
    Статус промиса может изменится только 1 раз
  */

  // let TestPromise = new Promise( function( resolveFunc, rejectFunc ){
  //  setTimeout( () => {
  //    // переведёт промис в состояние fulfilled с результатом "result"
  //    resolveFunc("result");
  //    // переведёт промис в состояние Rejected с результатом "ERROR 404"
  //    rejectFunc('ERROR 404');
  //  }, 1000);
  // });
  //
  // console.log( TestPromise );
  // TestPromise.then(
  //   // В then можем передать две функции - для обработки упешного состояния и для обработки ошибки
  //   res => {
  //     console.log('Fulfilled: ' + res );
  //     console.log( TestPromise );
  //   },
  //   error => {
  //     console.log( TestPromise );
  //     throw new Error('Rejected: ' + error);
  //   }
  // );

  /* LOAD CAT PROMISE */

  function loadImagePromise( url ){
    return new Promise( (resolve, reject) => {

      let imageElement = new Image();
          imageElement.onload = function(){
            resolve( imageElement );
          };
          imageElement.onerror = function(){
            let message = 'Error on image load at url ' + url;
            reject(
              RenderImage('images/cat5.jpg')
            );
          };

          imageElement.src = url;
    });
  }
  // loadImagePromise('asdasd');
  loadImagePromise('images/cat1.jpg').then((img) => {
    RenderImage(img.src);
  });





  // Promise.all([
  //   loadImagePromise('images/cat1.jpg'),
  //   loadImagePromise('images/cat2.jpg'),
  //   loadImagePromise('images/cat3.jpg'),
  //   loadImagePromise('images/cat4.jpg'),
  //   loadImagePromise('images/cat5.jpg'),
  //   loadImagePromise('images/cat6.jpg'),
  //   loadImagePromise('images/cat7.jpg')
  // ]).then( images => {
  //   images.forEach( img => RenderImage( img.src ));
  // }).catch( error => new Error( error ));

  /* ASYNC PROMISE
    fetch("http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2").then(
      resolve = ( res ) => {
        return res.json();
      }
    ).then(
      (data) => {
        return data[0];
      }
    ).then(
      (data) => {
        console.log('data', data);
        return data.name;
      }
    ).then(
      (data) => {

        // show friends
        return fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2").then(
            (res) => res.json()
          ).then( dataJson => {
            return {
              name: data,
              friends: dataJson[0]
            };
          });

      }
    ).then(
      (after2Fetch) =>{
        console.log(after2Fetch);
      }
    )
    .catch(error => {
      console(error); // Error: Not Found
    });
  */
